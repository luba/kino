---
title: "R Notebook"
output: html_notebook
---

```{r}

movies <- c('13 друзей Оушена', 'Майор Гром: Чумной доктор', 
            'Миллиард', 'Шерлок Холмс')

smd1506 <- c(8, 1, 5, 3)

ivb1506 <- c(6, 2, 1, 4)

marks <- data.frame(movies = movies, ivb1506 = ivb1506, smd1506 = smd1506)

```

Загрузить данные из таблицы Excel
```{r}
library(readxl)

pollsFile <- "C:\\Luba\\kino\\polls.xlsx"

episodes <- list()

for (num in 1:4) {
  
  episodes[[num]] <- read_xlsx(pollsFile, sheet = num)
  
}

```



```{r}

# IVB
meanEpisodes1 <- c()
for (num in 1:4) {
  
  meanEpisodes1[num] <- mean(episodes[[num]]$ivb1506)
  
}

meanEpisodes1
cor(marks$ivb1506, meanEpisodes1, method = "spearman")
marks$ivb1506

# SMD
meanEpisodes2 <- c()
for (num in 1:4) {
  
  meanEpisodes2[num] <- mean(episodes[[num]]$smd1506)
  
}

meanEpisodes2
cor(marks$smd1506, meanEpisodes2, method = "spearman")
marks$smd1506

ep <- append(meanEpisodes1, meanEpisodes2)
m <- append(marks$ivb1506, marks$smd1506)
cc <- cor(ep, m, method = "spearman")

plot(ep, m, main = paste("Коэфф. корр. = ", round(cc, digits=2)), 
     xlab = "Ср. оценка поэпизодников", 
     ylab = "Вероятность похода в кино (1-10)")

```

Без нулей средняя оценка по эпизодам
```{r}
# IVB
meanEpisodes1 <- c()
for (num in 1:4) {
  
  above0 <- episodes[[num]]$ivb1506 > 0
  meanEpisodes1[num] <- mean(episodes[[num]]$ivb1506[above0])
  
}

meanEpisodes1
cor(marks$ivb1506, meanEpisodes1, method = "spearman")
marks$ivb1506

# SMD
meanEpisodes2 <- c()
for (num in 1:4) {
  
  above0 <- episodes[[num]]$smd1506 > 0
  meanEpisodes2[num] <- mean(episodes[[num]]$smd1506[above0])
  
}

meanEpisodes2
cor(marks$smd1506, meanEpisodes2, method = "spearman")
marks$smd1506

ep <- append(meanEpisodes1, meanEpisodes2)
m <- append(marks$ivb1506, marks$smd1506)
cc <- cor(ep, m, method = "spearman") # 

plot(ep, m, main = paste("Оценки эпизодов >0. Коэфф. корр. = ", 
  round(cc, digits=2)), 
  xlab = "Ср. оценка эпизода", 
  ylab = "Шанс похода в кино (1-10)")


```
Доля запомненных эпизодов и ее влияние на вероятность пойти в кино
на этот фильм
```{r}
# IVB
recallEpisodes1 <- c()
for (num in 1:4) {
  
  above0 <- episodes[[num]]$ivb1506 > 0
  recallEpisodes1[num] <- sum(above0)/length(above0)
  
}

recallEpisodes1
cor(marks$ivb1506, recallEpisodes1, method = "spearman")
marks$ivb1506

# SMD
recallEpisodes2 <- c()
for (num in 1:4) {
  
  above0 <- episodes[[num]]$smd1506 > 0
  recallEpisodes2[num] <- sum(above0)/length(above0)
  
}

recallEpisodes2
cor(marks$smd1506, recallEpisodes2, method = "spearman")
marks$smd1506

ep <- append(recallEpisodes1, recallEpisodes2)
m <- append(marks$ivb1506, marks$smd1506)
cc <- cor(ep, m, method = "spearman")

plot(ep, m, main = paste("Оценки эпизодов >0. Коэфф. корр. = ", 
  round(cc, digits=2)), 
  xlab = "Доля запомненных эпизодов (оценка 1-5)", 
  ylab = "Вероятность похода в кино (1-10)")

```
Взаимосвязаны ли оценка эпизода с долей запомненных эпизодов

```{r}
# IVB
recallNum1 <- c()
recallMean1 <- c()
for (num in 1:4) {
  
  above0 <- episodes[[num]]$ivb1506 > 0
  recallNum1[num] <- sum(above0)/length(above0)
  recallMean1[num] <- mean(episodes[[num]]$ivb1506[above0])
  
}

recallEpisodes1
cor(marks$ivb1506, recallNum1, method = "spearman")
marks$ivb1506

# SMD
recallNum2 <- c()
recallMean2 <- c()
for (num in 1:4) {
  
  above0 <- episodes[[num]]$smd1506 > 0
  recallNum2[num] <- sum(above0)/length(above0)
  recallMean2[num] <- mean(episodes[[num]]$smd1506[above0])
  
}

recallEpisodes2
cor(marks$smd1506, recallNum2, method = "spearman")
marks$smd1506

recall <- append(recallNum1, recallNum2)
per <- append(recallMean1, recallMean2)
cc <- cor(recall, per, method = "spearman")

plot(recall, per, main = paste("Коэфф. корр. = ", 
  round(cc, digits=2)), 
  xlab = "Доля запомненных эпизодов", 
  ylab = "Средняя оценка эпизода (1-5)")
```





This is an [R Markdown](http://rmarkdown.rstudio.com) Notebook. When you execute code within the notebook, the results appear beneath the code. 

Try executing this chunk by clicking the *Run* button within the chunk or by placing your cursor inside it and pressing *Ctrl+Shift+Enter*. 

```{r}
plot(cars)
```

Add a new chunk by clicking the *Insert Chunk* button on the toolbar or by pressing *Ctrl+Alt+I*.

When you save the notebook, an HTML file containing the code and output will be saved alongside it (click the *Preview* button or press *Ctrl+Shift+K* to preview the HTML file).

The preview shows you a rendered HTML copy of the contents of the editor. Consequently, unlike *Knit*, *Preview* does not run any R code chunks. Instead, the output of the chunk when it was last run in the editor is displayed.
